from beers.models import Beer, Bar, Producer
from rest_framework import serializers


class BeerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Beer
        exclude = ('id',)
        depth = 1


class ProducerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Producer
        fields = '__all__'
        lookup_field = 'name'
        extra_kwargs = {
            'url': {'lookup_field': 'name'}
        }

class BarSerializer(serializers.ModelSerializer):
    class Meta:
        model = Bar
        fields = '__all__'
        depth = 1
