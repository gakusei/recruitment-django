# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase, APIClient
from django.contrib.auth.models import User
import json

from models import Producer, Beer, Bar


class BeerTests(APITestCase):
    def setUp(self):
        self.producer = Producer.objects.create(name="TestProducer")
        self.beer = Beer.objects.create(name="TestBeer", power=1, producer=self.producer)
        self.bar = Bar.objects.create(name="TestBar")
        self.bar.beers.add(self.beer)

    def test_get_producer_list(self):
        url = reverse('producer-list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_producer(self):
        response = self.client.get('/producers/TestProducer/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_create_producer(self):
        url = reverse('producer-list')
        data = {'name': 'TestProducer2'}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Producer.objects.count(), 2)
        self.assertEqual(Producer.objects.get(id=2).name, 'TestProducer2')

    def test_get_bar_list(self):
        url = reverse('bar-list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_bar(self):
        response = self.client.get('/bars/1/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_beer_list(self):
        url = reverse('beer-list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_beer(self):
        response = self.client.get('/beers/1/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
