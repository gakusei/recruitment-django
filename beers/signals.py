from django.db.models.signals import post_save
from django.dispatch import receiver
from beers.models import Beer, Producer
import hashlib


@receiver(post_save, sender=Beer)
def add_producer_id_hash(sender,  **kwargs):
    if kwargs.get('created'):
        beer = kwargs.get('instance')
        producer = Producer.objects.get(id=beer.producer.id)
        beer.producer_id_hash = hashlib.sha224(str(producer.id)).hexdigest()
        print producer.id
        print beer.producer_id_hash
        beer.save()
