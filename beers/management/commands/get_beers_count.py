from django.core.management.base import BaseCommand

from beers.models import Beer


class Command(BaseCommand):
    help = 'return number of beers in database'

    def handle(self, *args, **options):
        beer_number = Beer.objects.all().count()
        self.stdout.write(str(beer_number))
