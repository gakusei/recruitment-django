# -*- coding: utf-8 -*-
from rest_framework import viewsets
from django.db.models import Q, Count

from beers.models import Bar, Beer, Producer
from beers.serializers import BeerSerializer, BarSerializer, ProducerSerializer

"""
Rozwiązania Django ORM
1. podaj ile piw zapisanych jest w bazie danych

    >>> Beer.objects.all().count()

    Out: 157

2. wypisz wszystkich producentów piw z informacją ile każdy z nich wyprodukował piw
    
    >>> from django.db.models import Count
    >>> Beer.objects.all().values('producer').annotate(total=Count('producer')).order_by('producer_name')
    
    Out: <QuerySet [{'total': 19, 'producer__name': u'Browar Rodzinny'}, 
                    {'total': 14, 'producer__name': u'Browar Lomza'}, 
                    {'total': 23, 'producer__name': u'Browar Perla'}, 
                    {'total': 18, 'producer__name': u'Browar Aaaa'}, 
                    {'total': 21, 'producer__name': u'Browar Bbbb'}, 
                    {'total': 22, 'producer__name': u'Browar Cccc'}, 
                    {'total': 23, 'producer__name': u'Browar Dddd'}, 
                    {'total': 17, 'producer__name': u'Browar Eeee'}]
    
3. wypisz nazwe producenta piw ktory wyprodukowal najwiecej piw

    >>> from django.db.models import Count
    >>> Beer.objects.all().values('producer__name').annotate(total=Count('producer')).order_by('producer').latest('total') 
    
    Out: {'total': 23, 'producer__name': u'Browar Dddd'}                  
    
4. wypisz producentow ktorzy zdobyli wiecej niz 5 nagrod

    >>> Producer.objects.filter(prizes__gt=5).values('name', 'prizes')
    
    Out: <QuerySet [{'name': u'Browar Rodzinny', 'prizes': 8}, 
                    {'name': u'Browar Cccc', 'prizes': 6}]>

5. wypisz srednia moc piw

    >>> Beer.objects.all().aggregate(average_power=Avg('power')
    {'average_power': 5.189235668789805}

"""


class BeerViewSet(viewsets.ModelViewSet):
    queryset = Beer.objects.all()
    serializer_class = BeerSerializer


class BarViewSet(viewsets.ModelViewSet):
    queryset = Bar.objects.all().annotate(beer_number = Count('beers')).order_by('beer_number')
    serializer_class = BarSerializer


class ProducerViewSet(viewsets.ModelViewSet):
    queryset = Producer.objects.all().order_by('prizes')
    serializer_class = ProducerSerializer
    lookup_field = 'name'


class SpecialBeerViewSet(viewsets.ModelViewSet):
    queryset = Beer.objects.filter((Q(producer__id=1) & Q(power__gte=2) & Q(power__lte=5))
                                    | (Q(producer__id=2) & Q(power__gte=5) & Q(power__lte=9)))
    serializer_class = BeerSerializer
