from django.db import models

# Create your models here.


class Beer(models.Model):
    name = models.CharField(max_length=32, unique=True)
    power = models.DecimalField(max_digits=4, decimal_places=2)
    producer = models.ForeignKey('Producer')
    producer_id_hash = models.CharField(max_length=64, default="0", blank=True, null=True)


class Producer(models.Model):
    name = models.CharField(max_length=32, unique=True)
    prizes = models.IntegerField(default=0)


class Bar(models.Model):
    name = models.CharField(max_length=32)
    beers = models.ManyToManyField('Beer')
